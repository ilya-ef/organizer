// Core
// import axios from 'axios';

export const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';

export const api = Object.freeze({
    getVersion() {
        return '0.0.1';
    },
});
